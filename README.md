# ヒストリア
ヒストリアは歴史ストラテジーゲーム『Crusader Kings』の日本史Mod「NMIH」と「Shogunate」の歴史データを見やすい形で提供するプロジェクトです。家系図と連動してウィキペディアを表示できるのが特徴です。

![Sample Image](https://i.imgur.com/MEdS2Xv.png)

## 使用方法
Googleドライブから最新版の圧縮ファイルをダウンロードして、任意のフォルダに展開してください。次に、生成された `index.html` ファイルをお手元のブラウザで開いてください。

[**配布場所**](https://drive.google.com/drive/folders/1PJddZYGdc94IMY9mvLJhA6B09oYmuoQ_?usp=share_link)（ここから最新版をダウンロードしてください）

**ご注意**
- ダウンロード時に警告が表示されますが、中身はHTMLファイルなのでご安心ください。
- 小さなファイルが大量に生成されるので、展開先の空き容量にご注意ください。
- 最新版にアップデートする場合は、古いファイルを破棄して丸ごと差し替えてください。

## ヒント
- 人物目次の検索機能は遅いため、氏族目次を活用してください。
- 共有メモと修正依頼は外部リンクです。利用にはGitLabのアカウントが必要です。
- 共有メモの編集には開発者権限が必要です。希望される方は別途ご相談ください。

## 制限事項
- 日付は旧暦と新暦が混在しています。そのため、年齢には誤差が含まれます。
- ゲームの都合上、人名、生没年、家系図等に架空の設定を採用している場合があります。

## クレジット
ヒストリアは[Nova Monumenta Iaponiae Historica (NMIH)](https://steamcommunity.com/sharedfiles/filedetails/?id=333442855)のデータを作者のchatnoir17氏の許可のもと利用しています。

# Historia
Historia is a sub-project derived from the Shogunate and NMIH mods. It provides historical data on medieval Japan.

**Note:** Historia supports Japanese only at present.

## Usage
Download [the latest zip file](https://drive.google.com/drive/folders/1PJddZYGdc94IMY9mvLJhA6B09oYmuoQ_?usp=share_link) from Google Drive and extract it. Then open the `index.html` file with your browser.

**Note:** The archive contains many small files. Please be aware of your disk space.

## Credit
Historia uses [Nova Monumenta Iaponiae Historica (NMIH)](https://steamcommunity.com/sharedfiles/filedetails/?id=333442855) data with the author chatnoir17's permission.
