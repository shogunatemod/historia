import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input'    , help = 'input localization json file')
	parser.add_argument('output'   , help = 'output localization json file')
	parser.add_argument('trait'    , help = 'input trait table file')
	parser.add_argument('ck2'      , help = 'input CK2 localization folder')
	parser.add_argument('ck3'      , help = 'input CK3 localization folder')
	parser.add_argument('nmih'     , help = 'input NMIH localization folder')
	parser.add_argument('shogunate', help = 'input Shogunate localization folder')
	args = parser.parse_args()

	nmih_localization_dict = {}
	shogunate_localization_dict = {}
	trait_dict = {}

	# CK2

	for root, dirs, files in os.walk(args.ck2):
		for filename in files:
			print('Processing ' + filename + ' ...')

			jsonfile = open(root + '/' + filename, 'r', encoding = 'utf-8')
			temp_localization_dict = json.load(jsonfile, strict = False)
			jsonfile.close()

			for entry in temp_localization_dict:
				key   = entry['key']
				value = entry['translation']
				nmih_localization_dict[key] = value

	# CK3

	for root, dirs, files in os.walk(args.ck3):
		for filename in files:
			print('Processing ' + filename + ' ...')

			jsonfile = open(root + '/' + filename, 'r', encoding = 'utf-8')
			temp_localization_dict = json.load(jsonfile, strict = False)
			jsonfile.close()

			for entry in temp_localization_dict:
				key   = entry['key'][:-2]
				value = entry['translation']
				shogunate_localization_dict[key] = value

	# NMIH

	for root, dirs, files in os.walk(args.nmih):
		for filename in files:
			print('Processing ' + filename + ' ...')

			finp = open(root + '/' + filename, 'r', encoding = 'utf_8_sig')
			for line in finp:
				res = re.search(r'^\s*$', line)
				if res:
					continue
				res = re.search(r'^\s*#', line)
				if res:
					continue
				data = line.split(';')
				key   = data[0]
				value = data[1]
				nmih_localization_dict[key] = value
			finp.close()

	# Shogunate

	for root, dirs, files in os.walk(args.shogunate):
		for filename in files:
			print('Processing ' + filename + ' ...')

			finp = open(root + '/' + filename, 'r', encoding = 'utf_8_sig')
			for line in finp:
				res = re.search(r'^\s*#', line)
				if res:
					continue
				res = re.search(r'^\s+([^:]+):[0-9] \"([^\"]*)\"', line)
				if res:
					key   = res.group(1)
					value = res.group(2)
					shogunate_localization_dict[key] = value
			finp.close()

	# Trait Table

	finp = open(args.trait, 'r', encoding = 'utf_8_sig')
	for line in finp:
		res = re.search(r'^\s*#', line)
		if res:
			continue
		data = line.rstrip('\n').split(' ')
		if len(data) == 2:
			trait_dict[data[0]] = data[1]
	finp.close()

	# Read untranslated JSON file

	jsonfile = open(args.input, 'r', encoding = 'utf-8')
	localization_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	for key in localization_dict:
		if not localization_dict[key]['nmih']:
			if nmih_localization_dict.get(key):
				localization_dict[key]['nmih'] = nmih_localization_dict[key]
		if not localization_dict[key]['shogunate']:
			if shogunate_localization_dict.get(key):
				localization_dict[key]['shogunate'] = shogunate_localization_dict[key]
#			if trait_dict.get(key):
#				shogunate_key = 'trait_' + trait_dict[key]
#				if shogunate_localization_dict.get(shogunate_key):
#					localization_dict[key]['shogunate'] = shogunate_localization_dict[shogunate_key]

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(localization_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
