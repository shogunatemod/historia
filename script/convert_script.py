import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input' , help = 'input character data folder')
	parser.add_argument('output', help = 'output script json file')
	args = parser.parse_args()

	# Read character files

	source_dict = {}

	for root, dirs, files in os.walk(args.input):
		for filename in files:
			print('Processing ' + filename + ' ...')

			detector = UniversalDetector()
			with open(root + '/' + filename, mode='rb') as f:
				for binary in f:
					detector.feed(binary)
					if detector.done:
						break
			detector.close() 

			code = detector.result['encoding']

			finp = open(root + '/' + filename, 'r', encoding = code)

			charid = ''
			source = ''
			flag = False

			for line in finp:

				res = re.search(r'^([0-9]+)\s*=\s*\{', line)
				if res:
					flag = True
					charid = res.group(1)

				if flag:
					source = source + line.replace('\t', '    ')

				res = re.search(r'^\}', line)
				if res and flag:
					source_dict[charid] = source
					charid = ''
					source = ''
					flag = False

			finp.close()

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(source_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
