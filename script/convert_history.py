import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


index = 0
birth_year = 0
history_dict = {}


def history_start(charid, date, year):

	global index
	global history_dict

	index = index + 1
	entry = {}
	history_dict[charid].append(entry)
	history_dict[charid][index]['date'] = date + '（' + '{: >2}'.format(year - birth_year + 1) + '歳）'


def main():

	global index
	global birth_year
	global history_dict

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input'       , help = 'input character data folder')
	parser.add_argument('output'      , help = 'output history json file')
	parser.add_argument('localization', help = 'output localization json file')
	args = parser.parse_args()

	# Read character files

	history_list = []
	localization_dict = {}

	for root, dirs, files in os.walk(args.input):
		for filename in files:
			print('Processing ' + filename + ' ...')

			detector = UniversalDetector()
			with open(root + '/' + filename, mode='rb') as f:
				for binary in f:
					detector.feed(binary)
					if detector.done:
						break
			detector.close() 

			code = detector.result['encoding']

			finp = open(root + '/' + filename, 'r', encoding = code)

			charid = ''
			index  = -1
			history_flag = False

			for line in finp:

				res = re.search(r'^\s*#', line)
				if res:
					continue

				res = re.search(r'^([0-9]+)\s*=\s*\{', line)
				if res:
					charid = res.group(1)
					history_dict[charid] = []
					continue

				res = re.search(r'([0-9]+)\.([0-9]+)\.([0-9]+)\s*=\s*\{', line)
				if res:
					year  = int(res.group(1))
					month = int(res.group(2))
					day   = int(res.group(3))
					date  = '{: >4}'.format(year) + '年' + '{: >2}'.format(month) + '月' + '{: >2}'.format(day) + '日'
					history_flag = True
					continue

				res = re.search(r'^\}', line)
				if res:
					charid = ''
					index  = -1
					history_flag = False
					continue

				if not history_flag:
					continue

				# Birth & Death

				res = re.search(r'birth\s*=', line)
				if res:
					birth_year = year
					history_start(charid, date, year)
					history_dict[charid][index]['action'] = 'birth'

				res = re.search(r'death\s*=', line)
				if res:
					history_start(charid, date, year)
					history_dict[charid][index]['action'] = 'death'

				res = re.search(r'death_reason\s*=\s*\"*([0-9a-zA-Z_]+)\"*', line)
				if res:
					reason = res.group(1)
					history_dict[charid][index]['reason'] = reason

				res = re.search(r'killer\s*=\s*\"*([0-9a-zA-Z_]+)\"*', line)
				if res:
					history_dict[charid][index]['killer'] = res.group(1)

				# Trait

				res = re.search(r'add_trait\s*=\s*\"*([0-9a-zA-Z_]+)\"*', line)
				if res:
					trait = res.group(1)
					history_start(charid, date, year)
					history_dict[charid][index]['action'] = 'add_trait'
					history_dict[charid][index]['trait']  = trait

				res = re.search(r'remove_trait\s*=\s*\"*([0-9a-zA-Z_]+)\"*', line)
				if res:
					trait = res.group(1)
					if not ((trait[:2] == 'ju' or trait[:3] == 'sho') and (trait[-2:] == 'ge' or trait[-2:] == 'jo' or trait[-1:] == 'i')):
						history_start(charid, date, year)
						history_dict[charid][index]['action'] = 'remove_trait'
						history_dict[charid][index]['trait']  = trait

				# Marriage

				res = re.search(r'(add_spouse|remove_spouse|add_consort|remove_consort)\s*=\s*\"*([0-9a-zA-Z_]+)\"*', line)
				if res:
					action = res.group(1)
					person = res.group(2)
					history_start(charid, date, year)
					history_dict[charid][index]['action'] = action
					history_dict[charid][index]['person'] = person

				# Nickname

				res = re.search(r'give_nickname\s*=\s*\"*([0-9a-zA-Z_]+)\"*', line)
				if res:
					nickname = res.group(1)
					history_start(charid, date, year)
					history_dict[charid][index]['action'] = 'give_nickname'
					history_dict[charid][index]['nickname'] = nickname
#					if not nickname in history_list:
#						history_list.append(nickname)

			finp.close()

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(history_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()

	sorted_history = sorted(history_list)

	for history in sorted_history:
		localization_dict[history] = {}
		localization_dict[history]['nmih']      = ''
		localization_dict[history]['shogunate'] = ''

#	fout = open(args.localization, 'w', encoding = 'utf-8')
#	json.dump(localization_dict, fout, indent = 4, ensure_ascii = False)
#	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
