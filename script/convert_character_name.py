import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input' , help = 'input character name data folder')
	parser.add_argument('output', help = 'output character name json file')
	args = parser.parse_args()

	# Read localization file

	character_name_dict = {}

	finp = open(args.input, 'r', encoding = 'utf_8_sig')

	finp.readline()

	for line in finp:
		charid, date, english, japanese = line.rstrip('\n').split(',')
		character_name_dict[charid] = {}
		character_name_dict[charid]['english']  = english
		character_name_dict[charid]['japanese'] = japanese
	finp.close()

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(character_name_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
