import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input' , help = 'input character data folder')
	parser.add_argument('output', help = 'output character json file')
	args = parser.parse_args()

	# Read character files

	character_dict = {}

	for root, dirs, files in os.walk(args.input):
		for filename in files:
			print('Processing ' + filename + ' ...')

			detector = UniversalDetector()
			with open(root + '/' + filename, mode='rb') as f:
				for binary in f:
					detector.feed(binary)
					if detector.done:
						break
			detector.close() 

			code = detector.result['encoding']

			finp = open(root + '/' + filename, 'r', encoding = code)

			charid = ''
			dynastyid = ''
			birth_date = ''
			death_date = ''
			sex = 'male'
			history_flag = False

			for line in finp:

				res = re.search(r'^\s*#', line)
				if res:
					continue

				res = re.search(r'^([0-9]+)\s*=\s*\{', line)
				if res:
					charid = res.group(1)
					continue

				res = re.search(r'([0-9]+)\.([0-9]+)\.([0-9]+)\s*=\s*\{', line)
				if res:
					year  = int(res.group(1))
					month = int(res.group(2))
					day   = int(res.group(3))
					date  = str(year) + '.' + str(month) + '.' + str(day)
					history_flag = True
					continue

				res = re.search(r'birth\s*=', line)
				if res:
					birth_date = date
					continue

				res = re.search(r'death\s*=', line)
				if res:
					death_date = date
					continue

				res = re.search(r'dynasty\s*=\s*([0-9]+)', line)
				if res:
					dynastyid = res.group(1)
					continue

				res = re.search(r'female\s*=\s*yes', line)
				if res:
					sex = 'female'
					continue

				res = re.search(r'^\}', line)
				if res and charid:
					character_dict[charid] = {}
					character_dict[charid]['dynasty']    = dynastyid
					character_dict[charid]['birth_date'] = birth_date
					character_dict[charid]['death_date'] = death_date
					character_dict[charid]['sex']        = sex
					charid = ''
					dynastyid = ''
					birth_date = ''
					death_date = ''
					sex = 'male'
					history_flag = False
					continue

			finp.close()

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(character_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
