import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input' , help = 'input wikipedia data folder')
	parser.add_argument('output', help = 'output wikipedia json file')
	args = parser.parse_args()

	# Read wikipedia files

	wikipedia_dict = {}

	for root, dirs, files in os.walk(args.input):
		for filename in files:
			print('Processing ' + filename + ' ...')

			detector = UniversalDetector()
			with open(root + '/' + filename, mode='rb') as f:
				for binary in f:
					detector.feed(binary)
					if detector.done:
						break
			detector.close() 

			code = detector.result['encoding']

			finp = open(root + '/' + filename, 'r', encoding = code)

			res = re.search(r'^([0-9]+)\.txt', filename)
			if not res:
				continue
			else:
				charid = str(int(res.group(1)) - 10000000)

			title = finp.readline().rstrip('\n')
			url   = finp.readline().rstrip('\n')

			if charid and title and url:
				wikipedia_dict[charid] = {}
				wikipedia_dict[charid]['title'] = title
				wikipedia_dict[charid]['url']   = url

			finp.close()

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(wikipedia_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
