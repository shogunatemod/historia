import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input'       , help = 'input character data folder')
	parser.add_argument('output'      , help = 'output trait json file')
	parser.add_argument('localization', help = 'output localization json file')
	args = parser.parse_args()

	# Read character files

	trait_dict = {}
	trait_list = []
	localization_dict = {}

	for root, dirs, files in os.walk(args.input):
		for filename in files:
			print('Processing ' + filename + ' ...')

			detector = UniversalDetector()
			with open(root + '/' + filename, mode='rb') as f:
				for binary in f:
					detector.feed(binary)
					if detector.done:
						break
			detector.close() 

			code = detector.result['encoding']

			finp = open(root + '/' + filename, 'r', encoding = code)

			charid = ''
			religion = ''
			culture  = ''
			first_religion = ''
			first_culture  = ''
			traits = []
			history_flag = False

			for line in finp:

				res = re.search(r'^\s*#', line)
				if res:
					continue

				res = re.search(r'^([0-9]+)\s*=\s*\{', line)
				if res:
					charid = res.group(1)
					continue

				res = re.search(r'([0-9]+)\.([0-9]+)\.([0-9]+)\s*=\s*\{', line)
				if res:
					history_flag = True
					continue

				res = re.search(r'religion\s*=\s*\"*([^\"\s]+)\"*', line)
				if res:
					value = res.group(1)
					if not first_religion:
						first_religion = value
					religion = value
#					if not value in trait_list:
#						trait_list.append(value)
					continue

				res = re.search(r'culture\s*=\s*\"*([^\"\s]+)\"*', line)
				if res:
					value = res.group(1)
					if not first_culture:
						first_culture = value
					culture = value
#					if not value in trait_list:
#						trait_list.append(value)
					continue

				res = re.search(r'add_trait\s*=\s*\"*([^\"\s]+)\"*', line)
				if res and not history_flag:
					trait = res.group(1)
					if not trait in traits:
						traits.append(trait)
#					if not trait in trait_list:
#						trait_list.append(trait)
					continue

				res = re.search(r'^\}', line)
				if res and charid:
					trait_dict[charid] = {}
					trait_dict[charid]['first_religion'] = first_religion
					trait_dict[charid]['first_culture']  = first_culture
					trait_dict[charid]['religion']       = religion
					trait_dict[charid]['culture']        = culture
					trait_dict[charid]['traits']         = traits
					charid = ''
					religion = ''
					culture  = ''
					first_religion = ''
					first_culture  = ''
					traits = []
					history_flag = False
					continue

			finp.close()

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(trait_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()

	sorted_trait = sorted(trait_list)

	for trait in sorted_trait:
		localization_dict[trait] = {}
		localization_dict[trait]['nmih']      = ''
		localization_dict[trait]['shogunate'] = ''

	fout = open(args.localization, 'w', encoding = 'utf-8')
	json.dump(localization_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
