import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


character_dict = {}
character_name_dict = {}
dynasty_name_dict = {}


def get_first_name(charid, language='japanese'):

	if charid in character_name_dict:
		return character_name_dict[charid][language]
	else:
		return ''


def get_family_name(charid, language='japanese'):

	dynasty = character_dict[charid]['dynasty']
	if dynasty in dynasty_name_dict:
		return dynasty_name_dict[dynasty][language]
	else:
		return ''


def get_full_name(charid, language='japanese'):

	family_name = get_family_name(charid, language)
	first_name  = get_first_name (charid, language)

	if charid in character_dict:
		if (character_dict[charid]['dynasty'] == '420') or (character_dict[charid]['sex'] == 'female'):
			family_name = ''

	if language == 'japanese':
		return family_name + first_name
	else:
		return family_name + ' ' + first_name


def get_ui_name_no_link(charid):

	return get_full_name(charid, 'japanese') + ' / ' + get_full_name(charid, 'english')


def get_dynasty_name(dynastyid, language='japanese'):

	if dynastyid in dynasty_name_dict:
		dynasty_name = dynasty_name_dict[dynastyid][language]
		if language == 'japanese':
			if dynastyid == '420':
				dynasty_name = '天皇家'
			else:
				dynasty_name = dynasty_name + '氏'
		else:
			if dynastyid == '420':
				dynasty_name = 'Imperial family'
			else:
				dynasty_name = dynasty_name + ' clan'
	else:
		dynasty_name = ''

	return dynasty_name


def main():

	global character_dict
	global character_name_dict
	global dynasty_name_dict

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('data'          , help = 'input data folder')
	parser.add_argument('wiki'          , help = 'ouput wiki folder')
	parser.add_argument('character_data', help = 'input character data file')
	parser.add_argument('character_name', help = 'input character name data file')
	parser.add_argument('dynasty_name'  , help = 'input dynasty name data file')
	args = parser.parse_args()

	# Read data files

	jsonfile = open(args.data + '/' + args.character_data, 'r', encoding = 'utf-8')
	character_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.character_name, 'r', encoding = 'utf-8')
	character_name_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.dynasty_name, 'r', encoding = 'utf-8')
	dynasty_name_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	# Create wiki page

	for charid in character_dict:
		path = args.wiki + '/character/' + charid + '.md'
		if not os.path.exists(path):
			fout = open(path, 'w', encoding = 'utf-8')
			fout.write('# ' + get_ui_name_no_link(charid) + '\n')
			fout.close()

	for dynastyid in dynasty_name_dict:
		path = args.wiki + '/dynasty/' + dynastyid + '.md'
		if not os.path.exists(path):
			fout = open(path, 'w', encoding = 'utf-8')
			fout.write('# ' + get_dynasty_name(dynastyid) + '\n')
			fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
