import os
import re
import json
import argparse
from urllib.parse import quote
from chardet.universaldetector import UniversalDetector


project_url = 'https://gitlab.com/shogunatemod/historia/-/'

character_dict = {}
family_dict = {}
trait_dict = {}
history_dict = {}
localization_dict = {}
character_name_dict = {}
dynasty_name_dict = {}
wikipedia_dict = {}


def localize(text, mode='nmih'):

	if localization_dict.get(text):
		if localization_dict[text].get(mode):
			return localization_dict[text][mode]
	return text


def format_date(date):

	res = re.search(r'([0-9]+)\.([0-9]+)\.([0-9]+)', date)
	if res:
		year  = res.group(1).zfill(4)
		month = res.group(2).zfill(2)
		day   = res.group(3).zfill(2)
		return year + month + day
	else:
		return '0000' + '00' + '00'


def draw_family_tree(fout, target, charid, generation, end_generation):

	if (charid in character_dict) and (charid in family_dict):
		if (generation < end_generation) and ((generation < 0 and is_ancestor(target, charid, generation)) or (generation >= 0 and is_descendant(target, charid, generation))):
			fout.write('<div class="genealogy">\n')
			fout.write('<dl>\n')
			if target == charid:
				fout.write('<dt class="target">' + get_ui_name_no_roman(charid) + '</dt>\n')
			else:
				fout.write('<dt>' + get_ui_name_no_roman(charid) + '</dt>\n')
			for child in family_dict[charid]['children']:
				fout.write('<dd>')
				draw_family_tree(fout, target, child, generation + 1, end_generation)
				fout.write('</dd>\n')
			fout.write('</dl>\n')
			fout.write('</div>\n')
		else:
			fout.write(get_ui_name_no_roman(charid))


def is_ancestor(target, charid, generation):

	if charid == target:
		return True

	ancestor_flag = False

	if (charid in character_dict) and (charid in family_dict):
		if generation < 0 and len(family_dict[charid]['children']) > 0 and character_dict[charid]['sex'] == 'male':
			for child in family_dict[charid]['children']:
				if is_ancestor(target, child, generation + 1):
					ancestor_flag = True
					break

	return ancestor_flag


def is_descendant(target, charid, generation):

	if charid == target:
		return True

	descendant_flag = False

	if (charid in character_dict) and (charid in family_dict):
		if generation > 0:
			father = family_dict[charid]['father']
			mother = family_dict[charid]['mother']
			if father and is_descendant(target, father, generation - 1):
				descendant_flag = True
			if mother and is_descendant(target, mother, generation - 1):
				descendant_flag = True

	return descendant_flag


def get_first_name(charid, language='japanese'):

	if charid in character_name_dict:
		return character_name_dict[charid][language]
	else:
		return ''


def get_family_name(charid, language='japanese'):

	dynasty = character_dict[charid]['dynasty']
	if dynasty in dynasty_name_dict:
		return dynasty_name_dict[dynasty][language]
	else:
		return ''


def get_full_name(charid, language='japanese'):

	family_name = get_family_name(charid, language)
	first_name  = get_first_name (charid, language)

	if charid in character_dict:
		if (character_dict[charid]['dynasty'] == '420') or (character_dict[charid]['sex'] == 'female'):
			family_name = ''

	if language == 'japanese':
		return family_name + first_name
	else:
		return family_name + ' ' + first_name


def get_dynasty_name(dynastyid, language='japanese'):

	if dynastyid in dynasty_name_dict:
		dynasty_name = dynasty_name_dict[dynastyid][language]
		if language == 'japanese':
			if dynastyid == '420':
				dynasty_name = '天皇家'
			else:
				dynasty_name = dynasty_name + '氏'
		else:
			if dynastyid == '420':
				dynasty_name = 'Imperial family'
			else:
				dynasty_name = dynasty_name
	else:
		dynasty_name = ''

	return dynasty_name


def get_ui_name(charid):

	return add_link(charid, get_full_name(charid, 'japanese')) + ' / ' + get_full_name(charid, 'english')


def get_ui_name_no_link(charid):

	return get_full_name(charid, 'japanese') + ' / ' + get_full_name(charid, 'english')


def get_ui_name_no_roman(charid):

	return add_link(charid, get_full_name(charid, 'japanese'))


def get_ui_dynasty_name(dynastyid):

	return '<a href="../dynasty/' + dynastyid + '.html"><strong>' + get_dynasty_name(dynastyid) + '</strong></a> / ' + get_dynasty_name(dynastyid, 'english')


def get_nickname(charid, key):

	nickname = localize(key)
	if re.search(r'\[Root\.GetOnlyDynastyName\]', nickname):
		nickname = nickname.replace('[Root.GetOnlyDynastyName]', get_family_name(charid))
	if re.search(r'\[Root\.GetFirstName\]', nickname):
		nickname = nickname.replace('[Root.GetFirstName]', get_first_name(charid))
	return nickname


def get_birth_death(charid):

	birth_date = character_dict[charid]['birth_date'].split('.')
	death_date = character_dict[charid]['death_date'].split('.')

	age = str(int(death_date[0]) - int(birth_date[0]) + 1)

	birth_text = birth_date[0] + '年' + birth_date[1] + '月' + birth_date[2] + '日'
	death_text = death_date[0] + '年' + death_date[1] + '月' + death_date[2] + '日'

	return birth_text + '～' + death_text + '（享年' + age + '歳）'


def add_link(charid, text):

	if charid in wikipedia_dict:
		url = wikipedia_dict[charid]['url']
		return '<a href="../character/' + charid + '.html" class="wikipedia_link" onclick="parent.wikipedia.location.href=\'' + url + '\'">' + text + '</a>'
	else:
		return '<a href="../character/' + charid + '.html">' + text + '</a>'


def create_header(fp, mode, title, subtitle, dynastyid, charid):

	fp.write('<html>\n')
	fp.write('<head>\n')
	fp.write('<title>' + title + '</title>\n')
	fp.write('<link rel="stylesheet" type="text/css" href="../misc/historia.css">\n')
	fp.write('<script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>\n')
	fp.write('<script src="https://cdn.jsdelivr.net/gh/DeuxHuitHuit/quicksearch/dist/jquery.quicksearch.min.js"></script>\n')
	fp.write('<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?skin=sons-of-obsidian"></script>\n')
	fp.write('<script src="../misc/historia.js"></script>\n')

	fp.write('</head>\n')
	fp.write('<body>\n')

	fp.write('<div class="navigation">\n')

	if mode == 'cindex':
		fp.write('<span class="button_selected">人物目次</span>\n')
	else:
		fp.write('<a href="../character/index.html" class="button">人物目次</a>\n')

	if mode == 'dindex':
		fp.write('<span class="button_selected">氏族目次</span>\n')
	else:
		fp.write('<a href="../dynasty/index.html" class="button">氏族目次</a>\n')

	if mode == 'dynasty':
		fp.write('<span class="button_selected">' + get_dynasty_name(dynastyid) + '目次</span>\n')
	if mode == 'character':
		fp.write('<a href="../dynasty/' + dynastyid + '.html" class="button">' + get_dynasty_name(dynastyid) + '目次</a>\n')
		fp.write('<span class="button_selected">' + get_full_name(charid) + '</span>\n')

	fp.write('</div>\n')

	if mode == 'character':
		fp.write('<div class="navigation">\n')
		fp.write('<span class="button" id="button_tree">家系図</span>\n')
		fp.write('<span class="button" id="button_history">年表</span>\n')
		fp.write('<span class="button" id="button_data">特性</span>\n')
		fp.write('<span class="button" id="button_source">スクリプト</span>\n')
		fp.write('<a href="' + project_url + 'wikis/character/' + charid + '" target="_blank" class="button external_link">共有メモ</a>\n')
		fp.write('</div>\n')

	fp.write('<h1>' + title + '</h1>\n')

	if subtitle:
		fp.write('<h1 class="subtitle">' + subtitle + '</h1>\n')

	if mode == 'character':
		fp.write('<div class="issue">\n')
		fp.write('<a href="' + project_url + 'issues/new?issue[title]=' + quote('データ修正依頼（' + get_ui_name_no_link(charid) + ' / ' + charid + '）') + '&issuable_template=' + quote('Character Data') + '" target="_blank" class="button external_link">データ修正依頼</a>\n')
		fp.write('<a href="' + project_url + 'issues/new?issue[title]=' + quote('URL修正依頼（' + get_ui_name_no_link(charid) + ' / ' + charid + '）') + '&issuable_template=' + quote('Wikipedia') + '" target="_blank" class="button external_link">URL修正依頼</a>\n')
		fp.write('</div>\n')
		fp.write('<div class="spacer_character"></div>\n')


def create_footer(fp):

	fp.write('</body>\n')
	fp.write('</html>\n')
	fp.close()


def create_table_header(fp):

	fp.write('<table><tr><th>NMIH</th><th>Shogunate</th></tr>\n')


def create_table_row(fp, data1, data2):

	fp.write('<tr><td>' + data1 + '</td><td>' + data2 + '</td></tr>\n')


def create_history_row(fp, date, desc):

	date = date.replace(' ', '&nbsp;')
	fp.write('<tr><td class="date">' + date + '</td><td class="desc">' + desc + '</td></tr>\n')


def create_table_footer(fp):

	fp.write('</table>\n')


def create_search(fp):

	fp.write('<div class="search">\n')
	fp.write('<input type="text" id="keyword" placeholder="キーワードを入力して検索"/>\n')
	fp.write('<span class="loading">ロード中...</span>\n')
	fp.write('<button type="submit" id="submit"></button>\n')
	fp.write('</div>\n')
	fp.write('<ul id="list">\n')
	fp.write('<li id="noresults">見つかりませんでした</li>\n')


def main():

	global character_dict
	global family_dict
	global trait_dict
	global localization_dict
	global character_name_dict
	global dynasty_name_dict
	global wikipedia_dict
	global source_dict

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('data'          , help = 'input data folder')
	parser.add_argument('html'          , help = 'ouput html folder')
	parser.add_argument('character_data', help = 'input character data file')
	parser.add_argument('family_data'   , help = 'input family data file')
	parser.add_argument('trait_data'    , help = 'input trait data file')
	parser.add_argument('history_data'  , help = 'input history data file')
	parser.add_argument('localization'  , help = 'input localization data file')
	parser.add_argument('character_name', help = 'input character name data file')
	parser.add_argument('dynasty_name'  , help = 'input dynasty name data file')
	parser.add_argument('wikipedia'     , help = 'input wikipedia data file')
	parser.add_argument('source'        , help = 'input script source data file')
	args = parser.parse_args()

	# Read data files

	jsonfile = open(args.data + '/' + args.character_data, 'r', encoding = 'utf-8')
	character_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.family_data, 'r', encoding = 'utf-8')
	family_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.trait_data, 'r', encoding = 'utf-8')
	trait_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.history_data, 'r', encoding = 'utf-8')
	history_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.localization, 'r', encoding = 'utf-8')
	localization_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.character_name, 'r', encoding = 'utf-8')
	character_name_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.dynasty_name, 'r', encoding = 'utf-8')
	dynasty_name_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.wikipedia, 'r', encoding = 'utf-8')
	wikipedia_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	jsonfile = open(args.data + '/' + args.source, 'r', encoding = 'utf-8')
	source_dict = json.load(jsonfile, strict = False)
	jsonfile.close()

	# Create index page

	fidx = open(args.html + '/index.html', 'w', encoding = 'utf-8')

	fidx.write('<html>\n')
	fidx.write('<head>\n')
	fidx.write('<title>Historia</title>\n')
	fidx.write('<link rel="stylesheet" type="text/css" href="misc/historia.css">\n')
	fidx.write('</head>\n')

	fidx.write('<body>\n')
	fidx.write('<iframe id="pane_left" src="dynasty/index.html"></iframe>\n')
	fidx.write('<iframe id="pane_right" name="wikipedia" src="misc/README.html"></iframe>\n')
	fidx.write('</body>\n')
	fidx.write('</html>\n')

	fidx.close()

	# Create contents

	fcidx = open(args.html + '/character/index.html', 'w', encoding = 'utf-8')
	create_header(fcidx, 'cindex', '人物目次', '', '', '')
	create_search(fcidx)

	fdidx = open(args.html + '/dynasty/index.html', 'w', encoding = 'utf-8')
	create_header(fdidx, 'dindex', '氏族目次', '', '', '')
	create_search(fdidx)

	# Create HTML files

	sorted_dynastyid = sorted(dynasty_name_dict.keys(), key=int)

	for dynastyid in sorted_dynastyid:

		charlist = []
		for charid in character_dict:
			if character_dict[charid]['dynasty'] == dynastyid:
				charlist.append(charid)
		sorted_charid = sorted(charlist, key=int)

		if len(sorted_charid):

			print('Processing ' + get_dynasty_name(dynastyid) + ' (ID: ' + dynastyid + ') ...')

			fdidx.write('<li>' + get_ui_dynasty_name(dynastyid) + '</li>\n')

			fdyn = open(args.html + '/dynasty/' + dynastyid + '.html', 'w', encoding = 'utf-8')
			create_header(fdyn, 'dynasty', get_dynasty_name(dynastyid), '', dynastyid, '')
			create_search(fdyn)

			for charid in sorted_charid:

				birth_year = character_dict[charid]['birth_date'].split('.')[0]
				death_year = character_dict[charid]['death_date'].split('.')[0]
				character_text = get_ui_name(charid) + ' (' + birth_year + '～' + death_year + ')'

				fdyn .write('<li>' + character_text + '</li>\n')
				fcidx.write('<li>' + character_text + '</li>\n')

				fout = open(args.html + '/character/' + charid + '.html', 'w', encoding = 'utf-8')

				## Name & Nickname

				nickname = ''
				first_nickname = ''
				for history in history_dict[charid]:
					if history['action'] == 'give_nickname':
						if history.get('nickname'):
							nickname = get_nickname(charid, history['nickname'])
							if not first_nickname:
								first_nickname = nickname

				character_name = get_ui_name_no_link(charid)
				if nickname == first_nickname:
					create_header(fout, 'character', character_name, nickname, dynastyid, charid)
				else:
					create_header(fout, 'character', character_name, first_nickname + ' / ' + nickname, dynastyid, charid)


				### Family Tree

				fout.write('<div id="character_tree">\n')

				# Birth & Death Dates

				fout.write('<ul><li>生没年：')
				fout.write(get_birth_death(charid))
				fout.write('</li></ul>\n')

				# Family

				father      = family_dict[charid]['father']
				mother      = family_dict[charid]['mother']
				real_father = family_dict[charid]['real_father']
				real_mother = family_dict[charid]['real_mother']
				spouses     = family_dict[charid]['spouses']
				consorts    = family_dict[charid]['consorts']

				if (father in character_dict) or (mother in character_dict) or len(spouses) or len(consorts):

					fout.write('<ul>\n')

					if father in character_dict:
						if (real_father in character_dict) and (father != real_father):
							fout.write('<li>実父：' + get_ui_name(real_father) + '</li>\n')
							fout.write('<li>養父：' + get_ui_name(father) + '</li>\n')
						else:
							fout.write('<li>父：' + get_ui_name(father) + '</li>\n')
					if mother in character_dict:
						if (real_mother in character_dict) and (mother != real_mother):
							fout.write('<li>実母：' + get_ui_name(real_mother) + '</li>\n')
							fout.write('<li>養母：' + get_ui_name(mother) + '</li>\n')
						else:
							fout.write('<li>母：' + get_ui_name(mother) + '</li>\n')

					for spouse in spouses:
						if spouse in character_dict:
							if character_dict[charid]['sex'] == 'female':
								fout.write('<li>夫：' + get_ui_name(spouse) + '</li>\n')
							else:
								fout.write('<li>妻：' + get_ui_name(spouse) + '</li>\n')

					for consort in consorts:
						if consort in character_dict:
							fout.write('<li>側室：' + get_ui_name(consort) + '</li>\n')

					fout.write('</ul>\n')

				# Family Tree

				father = family_dict[charid]['father']
				if father in character_dict:
					grandfather = family_dict[father]['father']
					if grandfather in character_dict:
						draw_family_tree(fout, charid, grandfather, -2, 1)
					else:
						draw_family_tree(fout, charid, father, -1, 1)
				else:
					draw_family_tree(fout, charid, charid, 0, 1)

				fout.write('</div>\n')


				### History

				fout.write('<div id="character_history">\n')
				fout.write('<div class="spacer_history"></div>\n')
				fout.write('<table>\n')

				last_date = ''
				for history in history_dict[charid]:

					date    = history['date']
					action  = history['action']

					if date == last_date:
						date = ''
					else:
						last_date = date

					if action == 'birth':
						desc = '誕生'
						create_history_row(fout, date, desc)

					if action == 'death':
						desc = '死亡'
						if history.get('reason') and localize(history['reason']):
							if history.get('killer') and (history['killer'] in character_dict):
								desc = desc + '（' + localize(history['reason']) + '、' + get_ui_name_no_roman(history['killer']) + '）'
							else:
								desc = desc + '（' + localize(history['reason']) + '）'
						create_history_row(fout, date, desc)

					if action == 'add_trait' or action == 'remove_trait':
						if history.get('trait'):
							if localize(history['trait'], 'nmih') != localize(history['trait'], 'shogunate'):
								trait = '（<strong>' + localize(history['trait'], 'nmih') + ' / ' + localize(history['trait'], 'shogunate') + '</strong>）'
							else:
								trait = '（<strong>' + localize(history['trait']) + '</strong>）'
							if action == 'add_trait':
								desc = '特性獲得' + trait
							else:
								desc = '特性喪失' + trait
						create_history_row(fout, date, desc)

					if action == 'add_spouse' or action == 'remove_spouse' or action == 'add_consort':
						if history.get('person') and (history['person'] in character_dict):
							person = '（' + get_ui_name_no_roman(history['person']) + '）'
							if action == 'add_spouse':
								desc = '結婚' + person
							elif action == 'remove_spouse':
								desc = '離婚' + person
							elif action == 'add_consort':
								desc = '側室' + person
						create_history_row(fout, date, desc)

					if action == 'give_nickname':
						if history.get('nickname'):
							desc = '異名（<strong>' + get_nickname(charid, history['nickname']) + '</strong>）'
							create_history_row(fout, date, desc)

				fout.write('</table>\n')
				fout.write('<div class="spacer_character"></div>\n')
				fout.write('</div>\n')


				### Character Data

				fout.write('<div id="character_data">\n')

				if charid in trait_dict:

					# Religion & Culture

					fout.write('<h2>文化・宗教</h2>\n')
					create_table_header(fout)

					culture        = trait_dict[charid]['culture']
					religion       = trait_dict[charid]['religion']
					first_culture  = trait_dict[charid]['first_culture']
					first_religion = trait_dict[charid]['first_religion']

					if culture == first_culture:
						create_table_row(fout, localize(culture, 'nmih'), localize(culture, 'shogunate'))
					else:
						create_table_row(fout, localize(first_culture, 'nmih') + '→' + localize(culture, 'nmih'), localize(first_culture, 'shogunate') + '→' + localize(culture, 'shogunate'))

					if religion == first_religion:
						create_table_row(fout, localize(religion, 'nmih'), localize(religion, 'shogunate'))
					else:
						create_table_row(fout, localize(first_religion, 'nmih') + '→' + localize(religion, 'nmih'), localize(first_religion, 'shogunate') + '→' + localize(religion, 'shogunate'))

					create_table_footer(fout)

					# Trait

					if trait_dict[charid].get('traits'):

						fout.write('<h2>特性</h2>\n')
						create_table_header(fout)

						traits = trait_dict[charid]['traits']
						for trait in traits:
							create_table_row(fout, localize(trait, 'nmih'), localize(trait, 'shogunate'))

						create_table_footer(fout)

				fout.write('<div class="spacer_character"></div>\n')
				fout.write('</div>\n')


				### Script Source

				fout.write('<div id="character_source">\n')

				if charid in source_dict:
					fout.write('<div class="code"><pre class="prettyprint"><code>')
					fout.write(source_dict[charid])
					fout.write('</code></pre></div>\n')

				fout.write('<div class="spacer_character"></div>\n')
				fout.write('</div>\n')

				create_footer(fout)

			fdyn.write('</ul>\n')
			create_footer(fdyn)

	fcidx.write('</ul>\n')
	create_footer(fcidx)

	fdidx.write('</ul>\n')
	create_footer(fdidx)


if __name__ == "__main__":
	# execute only if run as a script
	main()
