import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input' , help = 'input dynasty name data folder')
	parser.add_argument('output', help = 'output dynasty name json file')
	args = parser.parse_args()

	# Read localization file

	dynasty_name_dict = {}

	finp = open(args.input, 'r', encoding = 'utf_8_sig')

	finp.readline()

	for line in finp:
		dynastyid, english, japanese = line.rstrip('\n').split(',')
		dynasty_name_dict[dynastyid] = {}
		dynasty_name_dict[dynastyid]['english']  = english
		dynasty_name_dict[dynastyid]['japanese'] = japanese
	finp.close()

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(dynasty_name_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
