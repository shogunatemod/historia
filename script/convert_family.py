import os
import re
import json
import argparse
from chardet.universaldetector import UniversalDetector


def main():

	# Parse Command Line Options
	parser = argparse.ArgumentParser()
	parser.add_argument('input' , help = 'input character data folder')
	parser.add_argument('output', help = 'output family json file')
	args = parser.parse_args()

	# Read character files

	character_dict = {}
	children_dict = {}

	for root, dirs, files in os.walk(args.input):
		for filename in files:
			print('Processing ' + filename + ' ...')

			detector = UniversalDetector()
			with open(root + '/' + filename, mode='rb') as f:
				for binary in f:
					detector.feed(binary)
					if detector.done:
						break
			detector.close() 

			code = detector.result['encoding']

			finp = open(root + '/' + filename, 'r', encoding = code)

			charid = ''
			father = ''
			mother = ''
			real_father = ''
			real_mother = ''
			spouses  = []
			consorts = []

			for line in finp:

				res = re.search(r'^\s*#', line)
				if res:
					continue

				res = re.search(r'^([0-9]+)\s*=\s*\{', line)
				if res:
					charid = res.group(1)
					continue

				res = re.search(r'father\s*=\s*([0-9]+)', line)
				if res:
					if not father:
						real_father = res.group(1)
					father = res.group(1)
					continue

				res = re.search(r'mother\s*=\s*([0-9]+)', line)
				if res:
					if not mother:
						real_mother = res.group(1)
					mother = res.group(1)
					continue

				res = re.search(r'add_spouse\s*=\s*([0-9]+)', line)
				if res:
					spouses.append(res.group(1))
					continue

				res = re.search(r'add_consort\s*=\s*([0-9]+)', line)
				if res:
					consorts.append(res.group(1))
					continue

				res = re.search(r'^\}', line)
				if res and charid:
					if not charid in character_dict:
						character_dict[charid] = {}
					character_dict[charid]['father'] = father
					character_dict[charid]['mother'] = mother
					character_dict[charid]['real_father'] = real_father
					character_dict[charid]['real_mother'] = real_mother
					character_dict[charid]['spouses']  = spouses
					character_dict[charid]['consorts'] = consorts
					if father:
						if not father in children_dict:
							children_dict[father] = [charid]
						else:
							children_dict[father].append(charid)
					if mother:
						if not mother in children_dict:
							children_dict[mother] = [charid]
						else:
							children_dict[mother].append(charid)
					charid = ''
					father = ''
					mother = ''
					real_father = ''
					real_mother = ''
					spouses  = []
					consorts = []
					continue

			finp.close()

	# Create family data

	for charid in character_dict:

		children = []
		if charid in children_dict:
			children = children_dict[charid]
		character_dict[charid]['children'] = children

		siblings = []
		father = character_dict[charid]['father']
		mother = character_dict[charid]['mother']
		if father and father in children_dict:
			for sibling in children_dict[father]:
				if sibling != charid and (not sibling in siblings):
					siblings.append(sibling)
		if mother and mother in children_dict:
			for sibling in children_dict[mother]:
				if sibling != charid and (not sibling in siblings):
					siblings.append(sibling)
		character_dict[charid]['siblings'] = siblings

		spouses = character_dict[charid]['spouses']
		for spouse in spouses:
			if spouse in character_dict and (not charid in character_dict[spouse]['spouses']):
				character_dict[spouse]['spouses'].append(charid)

	# Write json file

	fout = open(args.output, 'w', encoding = 'utf-8')
	json.dump(character_dict, fout, indent = 4, ensure_ascii = False)
	fout.close()


if __name__ == "__main__":
	# execute only if run as a script
	main()
